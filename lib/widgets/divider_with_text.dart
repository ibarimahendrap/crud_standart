import 'package:flutter/material.dart';

class DividerWithText extends StatelessWidget {
  const DividerWithText({
    Key key, this.text, this.colorText, this.colorDivider,
  }) : super(key: key);

  final String text;
  final Color colorText, colorDivider;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Divider(color: colorDivider),
          ),
        ),
        Text(text, style: TextStyle(color: colorText),),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Divider(color: colorDivider),
          ),
        ),
      ],
    );
  }
}