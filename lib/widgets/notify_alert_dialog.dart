import 'package:flutter/material.dart';

void notifyAlertDialog(context, text, color) {
  final Color colorDialog = color == 'success'
      ? Colors.green
      : (color == 'warning' ? Colors.orange : Colors.redAccent);
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            contentPadding: EdgeInsets.symmetric(horizontal: 15),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(7)),
            backgroundColor: colorDialog,
            content: 
            ListTile(
              contentPadding: EdgeInsets.only(left: 5),
              dense: false,
              title: Text(
                text.toString(),
                style: TextStyle(color: Colors.white),
              ),
              trailing: GestureDetector(
                child: Icon(Icons.close,color: Colors.white.withOpacity(.5), size: 16,),
                onTap: () {
                  Navigator.pop(context);
                }
              ),
            ),
          );
      });
}
