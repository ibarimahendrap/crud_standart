import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FormPasswordSign extends StatelessWidget {
  const FormPasswordSign({
    Key key, this.press, this.label,this.attr, this.togglePassword, this.require,
  }) : super(key: key);

  final Function press;
  final String label,attr;
  final bool togglePassword;
  final bool require;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(.2),
        borderRadius: BorderRadius.circular(15),
      ),
      child: FormBuilderTextField(
        name: attr,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        style: TextStyle(color: Colors.white.withOpacity(.7)),
        validator: FormBuilderValidators.compose([
          if(require) FormBuilderValidators.required(context),
          FormBuilderValidators.minLength(context,5),
        ]),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 25,vertical: 20),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.white.withOpacity(.3),style: BorderStyle.solid, width: 1.5)
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.indigo,style: BorderStyle.solid, width: 1.5)
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.redAccent,style: BorderStyle.solid, width: 1.5),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.redAccent,style: BorderStyle.solid, width: 1.5),
          ),
          fillColor: Colors.indigo,
          labelText: label,
          labelStyle: TextStyle(color: Colors.white.withOpacity(.5), fontSize: 17),
          suffixIcon: IconButton(
            onPressed: press,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            icon: togglePassword
              ? Icon(Icons.visibility_off,color: Colors.white.withOpacity(.5))
              : Icon(Icons.visibility,color: Colors.white.withOpacity(.5)),
          ),
        ),
        cursorColor: Colors.indigo,
        obscureText: togglePassword,
        keyboardType: TextInputType.text,
      ),
    );
  }
}