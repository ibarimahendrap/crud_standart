import 'package:flutter/material.dart';


class CircleIcon extends StatelessWidget {
  const CircleIcon({
    Key key, this.size, this.icon, this.colorBackground, this.colorIcon,
  }) : super(key: key);

  final double size;
  final icon;
  final Color colorBackground, colorIcon;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      minRadius: size,
      backgroundColor: Colors.white,
      child: Icon(icon,size: size+5, color: colorIcon),
    );
  }
}
