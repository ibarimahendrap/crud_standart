import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FormText extends StatelessWidget {
  const FormText({
    Key key, this.label, this.attr, this.require, this.icon, this.controller, this.type,
    // this.controller,
  }) : super(key: key);

  final String label, attr, type;
  final bool require;
  final icon;
  final controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: FormBuilderTextField(
        controller: controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        name: attr,
        validator: FormBuilderValidators.compose([
          if(require) FormBuilderValidators.required(context),
          if(attr == 'at_email') FormBuilderValidators.email(context),
          if(type == 'numb') FormBuilderValidators.numeric(context),
          if(attr == 'at_phone') FormBuilderValidators.minLength(context, 5),
        ]),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 25,vertical: 20),
          border: OutlineInputBorder(),
          labelText: label,
          prefixIcon: Icon(icon)
        ),
        keyboardType: type=="numb" ? TextInputType.number : TextInputType.text,
      ),
    );
  }
}