import 'package:flutter/material.dart';

void notifyButtomSheet(context,text,color){
  final Color colorDialog = color == 'success' ? Colors.green : (color == 'warning' ? Colors.orange : Colors.redAccent);
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (BuildContext bc) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 20,vertical: 40),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: colorDialog,
          borderRadius: BorderRadius.circular(7)
        ),
        child: Text(text.toString(), style: TextStyle(color: Colors.white),),
      );
    }
  );
}