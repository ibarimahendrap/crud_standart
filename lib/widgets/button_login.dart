import 'package:flutter/material.dart';
import 'package:crud_standart/constants/color.dart';

class ButtonLogin extends StatelessWidget {
  const ButtonLogin({
    Key key,
    this.press,
    this.title,
    this.flag,
  }) : super(key: key);

  final Function press;
  final String title;
  final bool flag;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Container(
        height: 50,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: flag ? Colors.transparent : Colors.white,
            borderRadius: BorderRadius.circular(15)),
        child: Text(title,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: flag ? Colors.white : cPrimary)),
      ),
    );
  }
}
