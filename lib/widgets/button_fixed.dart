import 'package:flutter/material.dart';

class ButtonFixed extends StatelessWidget {
  const ButtonFixed({
    Key key, this.text, this.colorBtn, this.colorText, this.colorBorder = Colors.transparent, this.press,
  }) : super(key: key);

  final String text;
  final Color colorBtn, colorText, colorBorder;
  final Function press;


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: FlatButton(
        onPressed: press,
        padding: EdgeInsets.symmetric(vertical: 10),
        color: colorBtn,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7),
          side: BorderSide(color : colorBorder)
        ),
        child: Text(text, style: TextStyle(color: colorText, fontWeight: FontWeight.bold)),
      ),
    );
  }
}