import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class FormTextSign extends StatelessWidget {
  const FormTextSign({
    Key key, this.label, this.attr, this.require,
    // this.controller,
  }) : super(key: key);

  final String label, attr;
  final bool require;
  // final controller;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black.withOpacity(.2),
        borderRadius: BorderRadius.circular(15),
      ),
      child: FormBuilderTextField(
        // controller: controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        name: attr,
        style: TextStyle(color: Colors.white.withOpacity(.7)),
        validator: FormBuilderValidators.compose([
          if(require) FormBuilderValidators.required(context),
          if(attr == 'at_email') FormBuilderValidators.email(context)
        ]),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(horizontal: 25,vertical: 20),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.white.withOpacity(.3),style: BorderStyle.solid, width: 1.5)
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.indigo,style: BorderStyle.solid, width: 1.5)
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.redAccent,style: BorderStyle.solid, width: 1.5),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide: BorderSide(color: Colors.redAccent,style: BorderStyle.solid, width: 1.5),
          ),
          fillColor: Colors.indigo,
          labelText: label,
          labelStyle: TextStyle(color: Colors.white.withOpacity(.5), fontSize: 17),
        ),
        cursorColor: Colors.indigo,
        keyboardType: TextInputType.text,
      ),
    );
  }
}