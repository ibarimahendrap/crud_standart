import 'package:flutter/material.dart';


void confirmDialog(context, String title, Function confirm){
  showDialog(
    context: context,
    builder: (BuildContext context) {
      final size = MediaQuery.of(context).size;
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        title: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 15.0),
                child: Text(
                    title,
                    style: TextStyle(fontSize: 16.0)),
              ),
              Divider(height: 1),
              Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 5.0),
                    width: (size.width - 100) / 2,
                    child: FlatButton(
                      padding: EdgeInsets.zero,
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text('Tidak'),
                    ),
                  ),
                  Container(
                    width: 10,
                    child: Text("|",
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(color: Colors.black12)),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 5.0),
                    width: (size.width - 100) / 2,
                    child: FlatButton(
                      padding: EdgeInsets.zero,
                      onPressed: confirm,
                      // color: Colors.redAccent,
                      child: Text('Ya', style: TextStyle()),
                    ),
                  ),
                ],
              )
            ],
          )
        ),
        titlePadding: EdgeInsets.zero,
      );
    }
  );
}