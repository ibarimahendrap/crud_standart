import 'package:flutter/material.dart';

const Color kPrimary      = Color(0xFF006699);
const Color kButtonTitle  = Color(0xFF006699);

const Color cPrimary      = Color(0xFF1a1a1a);
const Color cPrimaryY     = Color(0xFF262626);
const Color cPrimary2     = Color(0xFF404040);