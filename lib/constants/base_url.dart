const String urlLogin = "https://support88.redsystem.id/ci/api_base/login";
const String urlRegister =
    "https://support88.redsystem.id/ci/api_base/register";
const String urlListMahasiswa =
    "https://support88.redsystem.id/ci/api_base/lmhs";
const String urlInsMahasiswa =
    "https://support88.redsystem.id/ci/api_base/imhs";
const String urlUpdMahasiswa =
    "https://support88.redsystem.id/ci/api_base/umhs";
const String urlDelMahasiswa =
    "https://support88.redsystem.id/ci/api_base/dmhs";
