import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:crud_standart/constants/base_url.dart';
import 'package:crud_standart/constants/color.dart';
import 'package:crud_standart/functions/count_down.dart';
import 'package:crud_standart/screens/home/home_page.dart';
import 'package:crud_standart/widgets/form_text.dart';
import 'package:crud_standart/widgets/notify_alert_dialog.dart';
import 'package:http/http.dart' as http;

class FormMahasiswaPage extends StatefulWidget {
  final data;
  FormMahasiswaPage({this.data});

  @override
  _FormMahasiswaPageState createState() => _FormMahasiswaPageState(data: data);
}

class _FormMahasiswaPageState extends State<FormMahasiswaPage> {
  final data;
  _FormMahasiswaPageState({this.data});

  final GlobalKey<FormBuilderState> _formBuilderKey =
      GlobalKey<FormBuilderState>();

  bool loadPage = false;

  TextEditingController _nameControl = TextEditingController();
  TextEditingController _phoneControl = TextEditingController();
  TextEditingController _addressControl = TextEditingController();

  Future<void> submitFormMahasiswa(baseUrl, data) async {
    setState(() => loadPage = true);
    try {
      http.Response response = await http.post(
        Uri.encodeFull(baseUrl),
        headers: {"Accept": "application/json"},
        body: data,
      );
      this.setState(() {
        var res = json.decode(response.body);

        if (res['flag'] == 1) {
          notifyAlertDialog(context, res['message'], 'success');
          countDown(context, HomePage(), 500, 2);
        } else {
          notifyAlertDialog(context, res['message'], 'success');
        }
      });
    } catch (e) {
      notifyAlertDialog(context, "System Error", 'danger');
      print(e.toString());
    }
    setState(() => loadPage = false);
  }

  @override
  void initState() {
    super.initState();
    _setForm();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: cPrimaryY,
              title: Text('Form Mahasiswa'),
            ),
            body: Container(
              child: FormBuilder(
                key: _formBuilderKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: 10),
                    FormText(
                      attr: "at_name",
                      label: "Full Name",
                      icon: Icons.person,
                      controller: _nameControl,
                      require: true,
                    ),
                    SizedBox(height: 7),
                    FormText(
                      attr: "at_phone",
                      label: "Phone",
                      icon: Icons.phone_iphone,
                      controller: _phoneControl,
                      type: "numb",
                      require: true,
                    ),
                    SizedBox(height: 7),
                    FormText(
                      attr: "at_address",
                      label: "Address",
                      icon: Icons.pin_drop,
                      controller: _addressControl,
                      require: true,
                    ),
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: RaisedButton(
                        onPressed: () {
                          _formBuilderKey.currentState.save();
                          Map<String, dynamic> dataRes = {};
                          dataRes.addAll(_formBuilderKey.currentState.value);

                          if (_formBuilderKey.currentState.saveAndValidate()) {
                            if (data == null) {
                              submitFormMahasiswa(urlInsMahasiswa, dataRes);
                            } else {
                              dataRes.addAll({"id": data['id']});
                              submitFormMahasiswa(urlUpdMahasiswa, dataRes);
                            }
                          }
                        },
                        child: Text(
                          'Simpan',
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.blueAccent,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        loadPage
            ? Container(
                color: Colors.white.withOpacity(.1),
                alignment: Alignment.center,
                child: CircularProgressIndicator(backgroundColor: Colors.white),
              )
            : SizedBox()
      ],
    );
  }

  _setForm() {
    if (data != null) {
      _nameControl.text = data['name'];
      _phoneControl.text = data['phone'];
      _addressControl.text = data['address'];
    }
  }
}
