import 'package:crud_standart/constants/init.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:crud_standart/screens/home/home_page.dart';
import 'package:crud_standart/screens/sign_in/sign_in_page.dart';

class AppWidget extends StatefulWidget {
  @override
  _AppWidgetState createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Project Base',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: (sess.read('isLogin') == null)
          ? SignInPage()
          : ((sess.read('isLogin')) ? HomePage() : SignInPage()),
    );
  }
}
