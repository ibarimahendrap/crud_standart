import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:crud_standart/constants/base_url.dart';
import 'package:crud_standart/constants/color.dart';
import 'package:crud_standart/functions/count_down.dart';
import 'package:crud_standart/widgets/button_login.dart';
import 'package:crud_standart/widgets/divider_with_text.dart';
import 'package:crud_standart/widgets/form_password_sign.dart';
import 'package:crud_standart/widgets/form_text_sign.dart';
import 'package:crud_standart/widgets/notify_alert_dialog.dart';
import 'package:http/http.dart' as http;

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormBuilderState> _formBuilderKey =
      GlobalKey<FormBuilderState>();

  bool loadPage = false, flagToggle1 = true, flagToggle2 = true;

  Future<void> signUp(baseUrl, data) async {
    setState(() => loadPage = true);
    try {
      http.Response response = await http.post(
        Uri.encodeFull(baseUrl),
        headers: {"Accept": "application/json"},
        body: data,
      );
      this.setState(() {
        print(response.body);
        var res = json.decode(response.body);
        if (res['flag'] == 1) {
          notifyAlertDialog(context, res['message'], "success");
          countDown(context, null, 500, 0);
        } else
          notifyAlertDialog(context, res['message'], "danger");
      });
    } catch (e) {
      notifyAlertDialog(context, "System Error", 'danger');
      print(e.toString());
    }
    setState(() => loadPage = false);
  }

  void _toggleVisibility1() {
    setState(() => flagToggle1 = !flagToggle1);
  }

  void _toggleVisibility2() {
    setState(() => flagToggle2 = !flagToggle2);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Stack(
        children: [
          Scaffold(
            appBar: AppBar(
              backgroundColor: cPrimary,
              elevation: 0,
            ),
            backgroundColor: cPrimary,
            body: Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: FormBuilder(
                key: _formBuilderKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    Text("Let's sign you up.",
                        style: TextStyle(
                            fontSize: 35,
                            color: Colors.white,
                            fontWeight: FontWeight.bold)),
                    SizedBox(height: 10),
                    RichText(
                        textAlign: TextAlign.left,
                        text: TextSpan(
                            style: TextStyle(
                                fontSize: 30,
                                color: Colors.white,
                                fontWeight: FontWeight.w300,
                                height: 1.4,
                                letterSpacing: 1),
                            children: [
                              TextSpan(text: "Welcome My App"),
                            ])),
                    SizedBox(height: 50),
                    FormTextSign(
                      attr: 'at_fullname',
                      label: 'Full Name',
                      require: true,
                    ),
                    SizedBox(height: 15),
                    FormTextSign(
                      attr: 'at_email',
                      label: 'Email',
                      require: true,
                    ),
                    SizedBox(height: 15),
                    FormPasswordSign(
                      press: () => _toggleVisibility1(),
                      label: 'Password',
                      attr: 'at_password',
                      togglePassword: flagToggle1,
                      require: true,
                    ),
                    SizedBox(height: 15),
                    FormPasswordSign(
                      press: () => _toggleVisibility2(),
                      label: 'Re Password',
                      attr: 'at_repassword',
                      togglePassword: flagToggle2,
                      require: true,
                    ),
                    Spacer(),
                    SizedBox(height: 10),
                    ButtonLogin(
                      press: () {
                        _formBuilderKey.currentState.save();
                        var data = _formBuilderKey.currentState.value;

                        if (_formBuilderKey.currentState.saveAndValidate()) {
                          if (data['at_password'] == data['at_repassword'])
                            signUp(urlRegister, data);
                          else
                            notifyAlertDialog(context,
                                "The Passwords Don't Match", "success");
                        }
                      },
                      title: 'Sign Up',
                      flag: false,
                    ),
                    SizedBox(height: 25),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50),
                      child: DividerWithText(
                          text: 'OR',
                          colorText: Colors.white,
                          colorDivider: Colors.white),
                    ),
                    SizedBox(height: 30),
                  ],
                ),
              ),
            ),
          ),
          loadPage
              ? Container(
                  color: Colors.white.withOpacity(.1),
                  alignment: Alignment.center,
                  child:
                      CircularProgressIndicator(backgroundColor: Colors.white),
                )
              : SizedBox()
        ],
      ),
    );
  }
}
