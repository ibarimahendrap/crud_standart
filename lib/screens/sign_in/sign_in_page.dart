import 'dart:convert';

import 'package:crud_standart/constants/init.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:crud_standart/constants/base_url.dart';
import 'package:crud_standart/constants/color.dart';
import 'package:crud_standart/functions/count_down.dart';
import 'package:crud_standart/screens/home/home_page.dart';
import 'package:crud_standart/screens/sign_up/sign_up_page.dart';
import 'package:crud_standart/widgets/button_login.dart';
import 'package:crud_standart/widgets/form_password_sign.dart';
import 'package:crud_standart/widgets/form_text_sign.dart';
import 'package:crud_standart/widgets/notify_alert_dialog.dart';
import 'package:http/http.dart' as http;

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  final GlobalKey<FormBuilderState> _formBuilderKey =
      GlobalKey<FormBuilderState>();

  bool loadPage = false, flagToggle = true;

  Future<void> signIn(baseUrl, data) async {
    setState(() => loadPage = true);
    try {
      http.Response response = await http.post(
        Uri.encodeFull(baseUrl),
        headers: {"Accept": "application/json"},
        body: data,
      );
      this.setState(() {
        var res = json.decode(response.body);
        if (res['flag'] == 1) {
          _setSession(res);

          notifyAlertDialog(context, res['message'], 'success');
          countDown(context, HomePage(), 500, 1);
        } else
          notifyAlertDialog(context, res['message'], 'danger');

        loadPage = false;
      });
    } catch (e) {
      notifyAlertDialog(context, "System Error", 'danger');
      print(e.toString());
    }
  }

  void _toggleVisibility() {
    setState(() => flagToggle = !flagToggle);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => null,
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Stack(
          children: [
            Scaffold(
              appBar: AppBar(
                backgroundColor: cPrimary,
                elevation: 0,
                automaticallyImplyLeading: false,
              ),
              backgroundColor: cPrimary,
              body: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: FormBuilder(
                  key: _formBuilderKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      Text("Let's sign you in.",
                          style: TextStyle(
                              fontSize: 35,
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                      SizedBox(height: 10),
                      RichText(
                          textAlign: TextAlign.left,
                          text: TextSpan(
                              style: TextStyle(
                                  fontSize: 30,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w300,
                                  height: 1.4,
                                  letterSpacing: 1),
                              children: [
                                TextSpan(text: "Welcome back,\n"),
                                TextSpan(text: "You've been missed"),
                              ])),
                      SizedBox(height: 50),
                      FormTextSign(
                        attr: 'at_email',
                        label: 'Email',
                        require: true,
                      ),
                      SizedBox(height: 15),
                      FormPasswordSign(
                        press: () => _toggleVisibility(),
                        label: 'Password',
                        attr: 'at_password',
                        togglePassword: flagToggle,
                        require: true,
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Don't have an account?",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white.withOpacity(.7))),
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                      builder: (context) => new SignUpPage()));
                            },
                            child: Text("Register",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: ButtonLogin(
                          press: () {
                            _formBuilderKey.currentState.save();
                            var data = _formBuilderKey.currentState.value;
                            if (_formBuilderKey.currentState.saveAndValidate())
                              signIn(urlLogin, data);
                            // signIn(urlLogin, data);
                          },
                          title: 'Sign In',
                          flag: false,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            loadPage
                ? Container(
                    color: Colors.white.withOpacity(.1),
                    alignment: Alignment.center,
                    child: CircularProgressIndicator(
                      backgroundColor: Colors.white,
                    ),
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }

  _setSession(res) {
    sess.write('isLogin', true);
    sess.write('id_user', res['data']['id']);
  }
}
