import 'dart:convert';

import 'package:crud_standart/widgets/notify_alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:crud_standart/constants/base_url.dart';
import 'package:crud_standart/constants/color.dart';
import 'package:crud_standart/functions/sign_out.dart';
import 'package:crud_standart/screens/form_mahasiswa/form_mahasiswa_page.dart';
import 'package:crud_standart/screens/home/components/list_mahasiswa.dart';
import 'package:crud_standart/widgets/confirm_dialog.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<NavigatorState> _yourKey = GlobalKey<NavigatorState>();

  bool loadPage = false, flagToggle = true;

  List listMahasiswa = [];

  Future<void> getMahasiswa(baseUrl, load) async {
    if (load) {
      setState(() => loadPage = true);
    }
    try {
      http.Response response = await http.post(
        Uri.encodeFull(baseUrl),
        headers: {"Accept": "application/json"},
        body: {},
      );
      this.setState(() {
        listMahasiswa = json.decode(response.body);
      });
    } catch (e) {
      notifyAlertDialog(context, "System Error", 'danger');
      print(e.toString());
    }

    setState(() => loadPage = false);
  }

  Future<void> delMahasiswa(baseUrl, data) async {
    print(data);
    setState(() => loadPage = true);
    try {
      http.Response response = await http.post(
        Uri.encodeFull(baseUrl),
        headers: {"Accept": "application/json"},
        body: {'id': data['id']},
      );
      this.setState(() {
        var res = json.decode(response.body);
        getMahasiswa(urlListMahasiswa, false);
      });
    } catch (e) {
      notifyAlertDialog(context, "System Error", 'danger');
      print(e);
      setState(() => loadPage = false);
    }
  }

  @override
  void initState() {
    getMahasiswa(urlListMahasiswa, true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => null,
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: cPrimaryY,
              title: Text('List Mahasiswa'),
              automaticallyImplyLeading: false,
              actions: [
                IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () => getMahasiswa(urlListMahasiswa, true),
                ),
                IconButton(
                    icon: Icon(Icons.logout),
                    onPressed: () {
                      confirmDialog(
                          context,
                          'Apakah kamu yakin ingin keluar dari aplikasi?',
                          () => signOut(context));
                    })
              ],
            ),
            floatingActionButton: FloatingActionButton(
              backgroundColor: cPrimary2,
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                    builder: (context) => new FormMahasiswaPage(data: null),
                  ),
                ).then((value) => getMahasiswa(urlListMahasiswa, true));
              },
            ),
            body: Container(
              child: RefreshIndicator(
                onRefresh: () => getMahasiswa(urlListMahasiswa, false),
                child: ListView.builder(
                  itemCount: listMahasiswa == null ? 0 : listMahasiswa.length,
                  itemBuilder: (context, index) {
                    return ListMahasiswa(
                      index: index,
                      data: listMahasiswa[index],
                      pressUpd: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => new FormMahasiswaPage(
                                    data: listMahasiswa[index]))).then(
                            (value) => getMahasiswa(urlListMahasiswa, true));
                      },
                      pressDel: () {
                        delMahasiswa(urlDelMahasiswa, listMahasiswa[index]);
                      },
                    );
                  },
                ),
              ),
            ),
          ),
          loadPage
              ? Container(
                  color: Colors.white.withOpacity(.1),
                  alignment: Alignment.center,
                  child:
                      CircularProgressIndicator(backgroundColor: Colors.white),
                )
              : SizedBox()
        ],
      ),
    );
  }
}
