import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:crud_standart/constants/color.dart';


class ListMahasiswa extends StatelessWidget {
  const ListMahasiswa({
    Key key,
    this.index,
    this.data,
    this.pressUpd,
    this.pressDel,
  }) : super(key: key);

  final int index;
  final data;
  final Function pressUpd, pressDel;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Slidable(
      key: ValueKey(index),
      actionPane: SlidableDrawerActionPane(),
      secondaryActions: [
        IconSlideAction(
          color: Colors.green,
          iconWidget: Icon(Icons.edit, color: Colors.white),
          onTap: pressUpd,
        ),
        IconSlideAction(
          color: Colors.redAccent,
          iconWidget: Icon(Icons.delete, color: Colors.white),
          onTap: pressDel,
        ),
      ],
      child: Column(
        children: [
          index == 0 ? SizedBox() : Divider(height: 1),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              children: [
                Container(
                  width: 50,
                  height: 50,
                  child: CircleAvatar(
                    backgroundColor: cPrimary2,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Container(
                  width: (size.width - 50) - 70,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(data['name'],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold)),
                      SizedBox(
                        height: 5,
                      ),
                      Text(data['phone'],
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.black.withOpacity(.7),
                              fontWeight: FontWeight.w300)),
                      SizedBox(height: 1),
                      Text(data['address'],
                          style: TextStyle(
                              fontSize: 13,
                              color: Colors.black.withOpacity(.7),
                              fontWeight: FontWeight.w300)),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
