import 'dart:async';

import 'package:flutter/material.dart';

countDown(context, page, time, flag) async{
  var duration = Duration(milliseconds: time);
  return Timer(duration, (){
    if(flag == 1)
      Navigator.push(context,MaterialPageRoute(builder: (context) => page));
    else if(flag == 2)
      Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => page));
    else{
      int count = 0;
      Navigator.popUntil(context, (route) {
          return count++ == 2;
      });
    }
  });
}