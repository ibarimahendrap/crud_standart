import 'package:crud_standart/constants/init.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:crud_standart/screens/sign_in/sign_in_page.dart';

signOut(context) {
  sess.erase();

  Navigator.push(
      context, MaterialPageRoute(builder: (context) => SignInPage()));
}
